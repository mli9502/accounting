import Vue from 'vue'
import Router from 'vue-router'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-default/index.css'
import RecordList from '@/components/RecordList.vue'
import AddRecordPanel from '@/components/AddRecordPanel.vue'

Vue.use(Router)
Vue.use(ElementUI, { locale })

export default new Router({
  routes: [
    {
      path: '/',
      name: 'RecordList',
      component: RecordList
    },
    {
      path: '/add-record',
      name: 'AddRecordPanel',
      component: AddRecordPanel
    }
  ]
})
