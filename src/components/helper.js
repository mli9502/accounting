export function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time))
}

export function pad (n, width, char) {
  char = char || '0'
  n = n + ''
  return n.length >= width ? n : new Array(width - n.length + 1).join(char) + n
}
