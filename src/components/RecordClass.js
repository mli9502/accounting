'use strict'

class RecordClass {
  constructor () {
    this._id = 0
    this._date = ''
    this._amount = 0
    this._type = ''
    this._subType = ''
    this._description = ''
  }
  get amount () {
    return this._amount
  }
  set amount (amount_) {
    this._amount = amount_
  }
  setData (date, amount, type, subType, description) {
    console.log('Setting data in RecordClass ...')
    this._date = date
    this._amount = amount
    this._type = type
    this._subType = subType
    this._description = description
  }
  setId (id) {
    this._id = id
  }
}

export { RecordClass }
